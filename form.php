<?php
// check if fields passed are empty
if(empty($_POST['name'])                  ||
   empty($_POST['email'])                 ||
   empty($_POST['message'])        ||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
        echo "No arguments Provided!";
        return false;
   }
        
$name = $_POST['name'];
$email_address = $_POST['email'];
$message = $_POST['message'];
        
// create email body and send it        
$to = 'pierreyves@lecat-tapissier.fr'; // put your email
$email_subject = "LECAT - Vous avez un message de : $name";
$email_body = $message;
$headers = "LECAT - Vous avez un message de $name\n";       
mail($to,$email_subject,$email_body,$headers);
return true;                        
?>
